const express = require('express')
const path = require("path");
const bodyParser = require('body-parser')

const app = express()
app.use(express.json());
app.use(express.urlencoded());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.json())
let siswa = [
    {
        id:1,
        name: 'Haekal',
        address: 'Medan',
        email: 'hkl@gmail.com'
    },
    {
        id:2,
        name: 'mora',
        address: 'Depok',
        email: 'mrhakim@gmail.com'
    },
    {
        id:3,
        name: 'khuzain',
        address: 'banjarmasin',
        email: 'zain@gmail.com'
    }
]
let mentor = [
    {
        id: 1,
        name : 'zaki arsyad',
        address: 'jakarta'
    },
    {
        id:2,
        name: 'ahmed',
        address: 'surabaya'
    }
]

app.get('/', (req, res) => {
    res.render("index")
})

app.get('/student', (req,res) => {
    res.render("student", {studentlist: siswa} );
} )

app.get('/teacher', (req,res) => {
    res.render("teacher", {Teacherlist: mentor});
})

// app.get('/student/:name', (req, res) => {
//     const result = siswa.filter(hsl => {
//         return hsl.name.toLocaleLowerCase() === req.params.name.toLocaleLowerCase();
//     })
//     return res.json(result);
// } )
// app.get('/teacher/:name', (req, res) => {
//     const hasil = mentor.filter(hsl => {
//         return hsl.name.toLocaleLowerCase() === req.params.name.toLocaleLowerCase();
//     })
//     return res.json(hasil);
// } )


app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`);
  })